var http = require('http');
var url = require('url');

var server = http.createServer(function(request, response) {
    var page = url.parse(request.url).pathname;
    console.log(page);
    response.writeHead(200, {"Content-Type": "text/plain; charset=utf-8"});
    if (page == '/') {
        response.write('Vous avez trouvé la porte d\'entrée');
    }
    else if (page == '/famille') {
        response.write('Bienvenue dans notre famille');
    }
    else if (page == '/conference/21/12/2017') {
        response.write('Vous êtes bien à la conférence sur NodeJS et ExpressJS');
    }else{
        response.write('Je pense que vous vous êtes perdu en chemin !');
    }
    response.end();
});
server.listen(8080);