var express = require('express');

var server = express();

server.get('/', function (request, response) {
    response.setHeader("Content-Type", "text/plain; charset=utf-8");
    response.end('Vous avez trouvé la porte d\'entrée');
});

server.get('/famille', function (request, response) {
    response.setHeader("Content-Type", "text/plain; charset=utf-8");
    response.end('Bienvenue dans notre famille');
});

server.get('/conference/21/12/2017', function (request, response) {
    response.setHeader("Content-Type", "text/plain; charset=utf-8");
    response.end('Vous êtes bien à la conférence sur NodeJS et ExpressJS');
});

server.use( function (request, response, next) {
    response.setHeader("Content-Type", "text/plain; charset=utf-8");
    response.status(404, 'Je pense que vous vous êtes perdu en chemin !');
});

server.listen(8080);